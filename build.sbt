name := "Lab4_BD"

version := "0.1"

scalaVersion := "2.12.9"

logLevel := Level.Error

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "2.4.0",
  "org.apache.zookeeper" % "zookeeper" % "3.4.14" % "compile"
)