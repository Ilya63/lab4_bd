package ph

import java.util.concurrent.Semaphore
import org.apache.zookeeper._
import scala.util.Random

case class Philosopher(id: Int,
                       hostPort: String,
                       root: String,
                       left: Semaphore,
                       right: Semaphore,
                       seats: Integer) extends Watcher {

  val zk = new ZooKeeper(hostPort, 3000, this)
  val mutex = new Object()
  val path: String = root + "/" + id.toString

  if (zk == null) throw new Exception("ZK is NULL.")

  override def process(event: WatchedEvent): Unit = {
    mutex.synchronized {
      mutex.notify()
    }
  }

  def doAction(action: String): Unit = {
    printf("Философ {%d} %s\n", id, action)
    Thread.sleep(Random.nextInt(50) * 100)
  }

  def philosopherActions(): Boolean ={
    mutex.synchronized {
      while (true) {
        zk.create(path, Array.emptyByteArray, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL)
        doAction("думает.")
        left.acquire()
        doAction("взял левую вилку.")
        right.acquire()
        doAction("взял правую вилку. Начал есть.")
        right.release()
        doAction("освободил правую вилку.")
        left.release()
        doAction("положил левую вилку. Думает.")
        zk.delete(path, -1)
        Thread.sleep(Random.nextInt(50) * 100)
        return true
      }
    }
    false
  }
}