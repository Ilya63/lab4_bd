package ph

import java.util.concurrent.Semaphore

object MainPhilosopher {
  def main(args: Array[String]): Unit = {
    val hostPort = "localhost:2181"
    val philosophersCount = 5
    val seats = philosophersCount - 1

    val forks = new Array[Semaphore](philosophersCount)
    val philosophers = new Array[Philosopher](philosophersCount)
    for (j <- 0 until philosophersCount){
      forks(j) = new Semaphore(1)
    }

    val threads = new Array[Thread](philosophersCount)
    for (id <- 0 until philosophersCount){
      threads(id) = new Thread(
        new Runnable {
          def run(): Unit = {
            val i = (id + 1) % philosophersCount
            if (id == philosophersCount - 1){
              philosophers(id) = Philosopher(id, hostPort, "/ph", forks(id), forks(i), seats)
            }
            else {
              philosophers(id) = Philosopher(id, hostPort, "/ph", forks(i), forks(id), seats)
            }
            philosophers(id).philosopherActions()
          }
        }
      )
      threads(id).setDaemon(false)
      threads(id).start()
    }
    for (id <- 0 until philosophersCount){
      threads(id).join()
    }

  }

}
